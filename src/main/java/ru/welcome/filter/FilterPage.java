package ru.welcome.filter;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by sabir on 16.01.2017.
 */
//@WebFilter(filterName = "FilterPage")
public class FilterPage implements Filter {
    private static final Logger LOGGER = LoggerFactory.getLogger(FilterPage.class);

    public void destroy() {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        LOGGER.info("method FilterPage().doFilter() start");
        HttpServletRequest httpRequest  = (HttpServletRequest)request;
        HttpServletResponse httpResponse = (HttpServletResponse)response;
        HttpSession session = httpRequest.getSession();
        String name = (String) session.getAttribute("name");
        if (name != null){
            httpResponse.sendRedirect("welcome");
        } else{
            chain.doFilter(request, response);
        }
    }

    public void init(FilterConfig config) throws ServletException {
    }
}
