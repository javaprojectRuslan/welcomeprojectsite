package ru.welcome.filter;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

/**
 * Created by sabir on 15.01.2017.
 */
public class ValidatePass {
    private static final Logger LOGGER = LoggerFactory.getLogger(ValidatePass.class);
    private static Connection connection = null;
    private static PreparedStatement statement = null;
    private static ResultSet resultSet = null;
    private final static String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS users(" +
            "name varchar(20) NOT NULL, " +
            "password varchar(45) NOT NULL, " +
            "PRIMARY KEY (name)) " +
            "ENGINE=InnoDB DEFAULT CHARSET=utf8;";
    private final static String  SELECT_DATA_SQL = "SELECT * FROM users WHERE name = ? AND password = ?;";

    public static boolean checkUser(String name,String password) throws SQLException {
        LOGGER.info("method ValidatePass().checkUser() start");
        boolean check = false;

        try{
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbpassword","root","root");

            statement = connection.prepareStatement(CREATE_TABLE);
            statement.executeUpdate();

            statement = connection.prepareStatement(SELECT_DATA_SQL);
            statement.setString(1, name);
            statement.setString(2, password);
            resultSet = statement.executeQuery();
            check = resultSet.next();

        }catch(Exception e) {
            e.printStackTrace();
            LOGGER.error("Error MySQLIntegrityConstraintViolationException in RegisterServlet().doPost!", e);

        }finally {
            resultSet.close();
            statement.close();
            connection.close();
        }
        return check;
    }
}


