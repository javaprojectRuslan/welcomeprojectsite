package ru.welcome.validators;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by sabir on 11.01.2017.
 */
public class LengthAndLanguageName {        // проверка на соответствие имени
    private static final Logger LOGGER = LoggerFactory.getLogger(LengthAndLanguageName.class);

    public boolean comparison(String a){
        LOGGER.info("method LengthAndLanguageName().comparison() start");
        char[] word = a.trim().toCharArray();
        boolean lang = true;
        int len = word.length;

        for (char aWord : word) {
            if (!lang) {
                break;
            } else if (aWord >= 48 & aWord <= 57 | aWord >= 65 & aWord <= 122) {
                lang = true;
            } else {
                lang = false;
            }
        }
        return lang & len > 4;
    }
}
