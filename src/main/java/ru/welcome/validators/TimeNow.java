package ru.welcome.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by sabir on 11.01.2017.
 */
public class TimeNow { // приветствие по времени суток на главной странице
    private static final Logger LOGGER = LoggerFactory.getLogger(TimeNow.class);

    public String hello(){

        LOGGER.info("method TimeNow().hello() start");
        Calendar calendar = new GregorianCalendar();
        int h = calendar.get(Calendar.HOUR_OF_DAY);
        String s;

        if (h >= 6 & h < 10){
            s = "Доброе утро, ";
        }else if (h >= 10 & h < 18){
            s = "Добрый день, ";
        }else if (h >=18 & h < 22){
            s = "Добрый вечер, ";
        }else {
            s = "Доброй ночи, ";
        }
        return s;
    }
}
