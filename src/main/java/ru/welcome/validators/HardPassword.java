package ru.welcome.validators;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by sabir on 11.01.2017.
 */
public class HardPassword {                       // проверка пароля на трудность
    private static final Logger LOGGER = LoggerFactory.getLogger(HardPassword.class);

    public boolean comparisonPassword(String a) {
        LOGGER.info("method HardPassword().comparisonPassword() start");
        char[] word = a.trim().toCharArray();
        int len = word.length;
        boolean number = true;
        boolean big = true;
        boolean smal = true;

        for (int i = 0; i < len; i++) {
            if (!number){
                break;
            }else if (word[i] >= 48 & word[i] <= 57 | word[i] >= 65 & word[i] <= 122) {
                number = true;
            }else {
                number = false;
            }
        }
        smal = !a.equals(a.toLowerCase());
        big = !a.equals(a.toUpperCase());
        return len >= 8 & number & big & smal;
    }
}
