package ru.welcome.validators;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by sabir on 11.01.2017.
 */
public class PasswordComparison {             // проверка идентичности паролей при регистрации
    private static final Logger LOGGER = LoggerFactory.getLogger(PasswordComparison.class);

    public boolean twin(String a, String b) {

        LOGGER.info("method PasswordComparison().twin() start");
        return a.trim().equals(b.trim());
    }
}

