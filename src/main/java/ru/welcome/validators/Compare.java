package ru.welcome.validators;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by sabir on 15.01.2017.
 */
public class Compare {
    private static final Logger LOGGER = LoggerFactory.getLogger(Compare.class);

    public String correctWord(String name, String passwordOne, String passwordTwo){
        LOGGER.info("method Compare().correctWord() start");
        String message;
        LengthAndLanguageName l = new LengthAndLanguageName();
        HardPassword h = new HardPassword();
        PasswordComparison p = new PasswordComparison();
        String error = "<html><head><title>Ошибка регистрации</title>";
        String color = "<font color='red'><tr>";
        String length =  "<td>Имя пользователя должно быть длинее 4 символов и состоять из цифр и букв английского алфавита!!!</td></tr><br>";
        String address = "<tr><td><a href=\"http://localhost:8080/sign-up\"> вернуться к регистрации</a></td></tr></table>";
        String valid = "<td>Пароль не достаточно сложен: должны быть цифры, заглавные и строчные буквы и длина минимум 8 символов!!!</td></tr><br>";
        String coincidence = "<tr><td>Пароль и повтор пароля не совпадают!!!</td></tr><br>";


        if (!l.comparison(name) & h.comparisonPassword(passwordOne) & p.twin(passwordOne, passwordTwo)) {
            message = error + color + length + address;

        } else if (l.comparison(name) & !h.comparisonPassword(passwordOne) & p.twin(passwordOne, passwordTwo)) {
            message = error + color + valid + address;

        } else if (l.comparison(name) & h.comparisonPassword(passwordOne) & !p.twin(passwordOne, passwordTwo)) {
            message = error + color + coincidence + address;

        } else if (!l.comparison(name) & !h.comparisonPassword(passwordOne) & p.twin(passwordOne, passwordTwo)) {
            message = error + color + length + valid + address;

        } else if (!l.comparison(name) & h.comparisonPassword(passwordOne) & !p.twin(passwordOne, passwordTwo)) {
            message = error + color + length + coincidence + address;

        } else if (l.comparison(name) & !h.comparisonPassword(passwordOne) & !p.twin(passwordOne, passwordTwo)) {
            message = error + color + valid + coincidence + address;

        } else {
            message = error + color + length + valid + coincidence + address;

        }
        return message;
    }
}
