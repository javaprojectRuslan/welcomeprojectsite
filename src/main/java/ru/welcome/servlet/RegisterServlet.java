package ru.welcome.servlet;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.welcome.validators.Compare;
import ru.welcome.validators.HardPassword;
import ru.welcome.validators.LengthAndLanguageName;
import ru.welcome.validators.PasswordComparison;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by sabir on 15.01.2017.
 */
//@WebServlet(name = "RegisterServlet")
public class RegisterServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegisterServlet.class);
    private final static String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS users(" +
            "name varchar(20) NOT NULL, " +
            "password varchar(45) NOT NULL, " +
            "PRIMARY KEY (name)) " +
            "ENGINE=InnoDB DEFAULT CHARSET=utf8;";
    private final static String  ADD_DATA_SQL = "INSERT INTO users (name, password) VALUE(?,?);";
    private static Connection connection = null;
    private static PreparedStatement statement = null;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        LOGGER.info("method RegisterServlet().doPost() start");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        String name = request.getParameter("nameOne");
        String passwordOne = request.getParameter("passwordOne");
        String passwordTwo = request.getParameter("passwordTwo");
        LengthAndLanguageName l = new LengthAndLanguageName();
        HardPassword h = new HardPassword();
        PasswordComparison p = new PasswordComparison();

        if ((l.comparison(name) & h.comparisonPassword(passwordOne) & p.twin(passwordOne, passwordTwo))) {

            try {
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbpassword", "root", "root");

                statement = connection.prepareStatement(CREATE_TABLE);
                statement.executeUpdate();

                statement = connection.prepareStatement(ADD_DATA_SQL);
                statement.setString(1, name);
                statement.setString(2, passwordOne);
                statement.executeUpdate();
                HttpSession session = request.getSession();
                session.setAttribute("name", name);
                response.sendRedirect("welcome");

            } catch (Exception e) {
                writer.println("<html><head><title>Ошибка регистрации</title>" +
                        "<font color='red'><tr><td>Пользователь с таким именем уже существует!!!</td></tr><br>");
                writer.println("<tr><td><a href=\"http://localhost:8080/sign-up\"> вернуться к регистрации</a></td></tr></table>");
                LOGGER.error("Error MySQLIntegrityConstraintViolationException in RegisterServlet().doPost!", e);
                writer.close();

            }finally {
                try {
                    writer.close();
                    connection.close();
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } else
            writer.println(new Compare().correctWord(name, passwordOne, passwordTwo));
            writer.close();
    }
}



