package ru.welcome.servlet;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.welcome.validators.TimeNow;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by sabir on 15.01.2017.
 */
//@WebServlet(name = "WelcomeServlet")
public class WelcomeServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(WelcomeServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info("method WelcomeServlet().doGet() start");
        response.setContentType("text/html; charset=UTF-8");
        PrintWriter writer = response.getWriter();
        HttpSession session = request.getSession();
        String name = (String) session.getAttribute("name");

        if (name != null) {
            writer.println("<html>");
            writer.println("<head>");
            writer.println("<title>Приветствие</title>");
            writer.println("</head>");
            writer.println("<body>");
            writer.println("<center><h2>" + new TimeNow().hello() + name + "!!!</h2>");
            writer.println("<br><form method=\"POST\"" + " action=\"out\"");
            writer.println("<td><input type=submit value=\"Выйти\" ></td></br>");
            writer.println("</table>");
            writer.println("</center>");
            writer.println("</body>");
            writer.println("</html>");
            writer.close();
        } else {
            response.sendRedirect("sign-in");
        }
    }
}
