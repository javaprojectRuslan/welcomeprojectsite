package ru.welcome.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.welcome.filter.ValidatePass;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

/**
 * Created by sabir on 15.01.2017.
 */
//@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginServlet.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info("method LoginServlet().doPost() start");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        Cookie cookie = new Cookie("MyCookie", name);
        cookie.setMaxAge(1000);
        response.addCookie(cookie);

        try {
            if(ValidatePass.checkUser(name, password)){
                HttpSession session = request.getSession();
                session.setAttribute("name", name);
    //            session.setMaxInactiveInterval(1000);
                response.sendRedirect("welcome");

            } else {
                writer.println("<font color='red'><tr><td>Имя пользователя или пароль не подходят, " +
                        "попробуйте ввести повторно или зарегистрируйтесь!</td></tr><br>");
                RequestDispatcher rs = request.getRequestDispatcher("");
                rs.include(request, response);
            }

        } catch (SQLException e) {
            e.printStackTrace();

        }finally {
            writer.close();
        }
    }
}
