package ru.welcome.servlet;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by sabir on 16.01.2017.
 */
//@WebServlet(name = "OutServlet")
public class OutServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(OutServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info("method OutServlet().doPost() start");
        request.getSession().invalidate();
        response.sendRedirect("/sign-in");
    }
}
